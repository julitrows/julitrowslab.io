---
layout: post
company: RubiconMD
link: https://www.rubiconmd.com
title: Principal Engineer
categories:
- resume
finish: present date
location: New York City (US), remotely from Seville (Spain)
technologies:
- Ruby on Rails
- Angular
- RSpec
- PostgreSQL
- OpenSearch
- CircleCI
- GraphQL
- Docker
- AWS
---

*RubiconMD is an innovative eConsult service that connects primary care providers to same-day insights from top medical specialists, aiming to democratize medical expertise.*

Hired as a mid-level Ruby on Rails developer, I focused on the backend and had key roles in the building of internal tools for the Operations team as well as the automation frameworks for internal product workflows, key to platform scaling.

Since then my professional growth has made my work evolve as well. I've participated in all the stages, from design to delivery, of solutions for feature requests from our Product Team, clients and partners, as well and scaling, security and performance requirements. I've also helped put in place organizational practices and processes that have helped us to better organize as a team.

On the human side, it's been my responsibility to be a bridge between the NYC office and the rest of Spanish developers, as a lead/coordinator and hiring mananger. I'm also mentoring and acting as technical manager for all new hires in the Backend Team, which I lead.
