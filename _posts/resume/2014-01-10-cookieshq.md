---
layout: post
company: CookiesHQ
link: http://cookieshq.co.uk
title: Fullstack Ruby on Rails developer
categories:
- resume
finish: July 2016
location: Bristol (UK) - Remotely from Seville (Spain)
projects:
  - Cardnest: https://www.cardnest.com
  - CookiesHQ: http://cookieshq.co.uk
  - Delivery Folk: http://deliveryfolk.co.uk
  - Digestive: http://digestive.io
  - Good Sixty: http://goodsixty.co.uk
  - My Gap Medics: https://www.mygapmedics.com
  - Patternbank: https://patternbank.com
  - Pulsenotes: https://pulsenotes.com
  - The Perfect Pitch: '!#'
  - TORF: http://torf.co.uk
  - Visible People: https://www.visiblepeople.com/
  - Warwire: http://warwire.net

technologies:
  - Ruby on Rails
  - Backbone.js
  - Bootstrap
  - Sass
  - SMACSS
  - Bourbon & Neat
  - PostgreSQL
  - MySQL
  - MongoDB
  - Engine Yard
  - Heroku
  - Nginx
  - TDD/BDD
  - Pair programming
  - Git
---
CookiesHQ is a web agency for which I worked on projects in every stage of development and maturity (bespoke CRM's, MVPs, legacy apps already in production) in direct contact with clients, and as well as technical writer for the company blog.
