---
layout: post
link: '!#'
title: Freelance Fullstack Ruby on Rails developer
categories:
- resume
company: Salumedia, Redradix and Emergya
finish: May 2014
location: Seville (Spain), Remote
projects:
  - The Global School-based Student Health Survey: http://gshs.salumedia.com/
  - Formularios Salud y Escuela: http://sam-gshs.salumedia.com/users/sign_in
  - Sjekk Deg: http://www.sjekkdeg.no/welcome
  - Occidental Hotels: http://www.occidentalhotels.com/

technologies:
  - Ruby on Rails
  - Reveal.js
  - Sass
  - Git
  - TDD
  - Nginx
  - MySQL
  - PostgreSQL
  - Debian servers
  - Pair programming
  - Varnish
  - jRuby
  - Bootstrap
  - SVN
  - Oracle Enterprise DB
  - Centralized Authorization Server
---
Adding functionalities and fix bugs in already existing projects, develop new projects from scratch, as well as upgrading/updating/managing servers and deploying applications.
