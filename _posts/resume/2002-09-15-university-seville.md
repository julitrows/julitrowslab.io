---
layout: post
institution: University of Seville (Spain)
link: https://www.informatica.us.es/
location: Seville (Spain)
title: MsC in Computer Science & Software Engineering<br>(Ingeniería Informática Plan 97)
categories:
- edu
finish: September 2009
---
Five year-long BsC degree later officially upgraded to a MsC per the EHEA Bologna Process.

Final Thesis: Ubuntu-based Linux Distribution intended for children, "Guadalinex Infantil".

Memberships:

* [US FLOSS Users Group](https://sugus.eii.us.es/)
* [IEEE Student Branch](https://ieeesb.us.es/)

Paid internships:

* Web Developer at Inix Consultores (5 months, 2008)
* C++ Developer at Robotics Vision and Control Group (18 months)
