---
layout: post
title: Chiquito Faker
categories:
- development
tags: [gems, faker, floss, contributions, chiquito]
---

The past National Day of Andalusia (Feb 28th) I paid homage to one of the greatest public figures this region of Spain has ever given to the world. I did it by itching a scratch I've had for several years:

> What if I could use Chiquito's jokes for generating random test data in my Ruby projects?

I've been user the Faker gem since forever, so adding a module to this already extensive library should be the way to go.

It turned out to be quite easy (never understimete the power of code precedence), and the [pull request](https://github.com/faker-ruby/faker/pull/1940) received lots of love and was seamlessly merged.

I wish contributing to FLOSS was this easy and fun all the time.
