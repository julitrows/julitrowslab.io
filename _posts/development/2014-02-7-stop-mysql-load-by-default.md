---
layout: post
title: Stop MySQL being loaded on OS X boot
categories:
- development
tags: [mysql, mariadb, homebrew, OS X, processes]
---

Right now I find myself working on a porject that uses MongoDB, and sometimes I work on projects with PostgreSQL and other times on projects that use MySQL. The difference is that I have to manually load both MongoDB (mongod) and PostgreSQL (I use the [Heroku app](http://postgresapp.com/)) when I want to use them, but MySQL is loaded in my system by default.

Thinking on an all-or-none solution, I decided to convert MySQL in a service run on demand, instead of a resident one. I have installed [MariaDB](https://mariadb.org/) via Homebrew, so:

{% highlight bash %}
  ➜  julitrows.gitlab.com git:(master) ✗ brew info mariadb
  mariadb: stable 5.5.34, devel 10.0.7
  http://mariadb.org/
  ...
  **BLAH BLAH BLAH **
  ...
  To load mariadb after an update
      launchctl unload ~/Library/LaunchAgents/homebrew.mxcl.mariadb.plist
      launchctl load ~/Library/LaunchAgents/homebrew.mxcl.mariadb.plist
{% endhighlight %}

So the second to last line is what we are looking for. By running:

``launchctl unload ~/Library/LaunchAgents/homebrew.mxcl.mariadb.plist``

We'll remove mysql from being run at startup.

And when we want to use mysql, just do ``mysql.server start``, and ``mysql.server stop``

Easy!
