---
layout: post
title: Capistrano-Tasks gem released
categories:
- development
tags:
- releases
- capistrano
- cookbook
- gem
status: publish
type: post
published: true
---
I've put up a teeny little gem which compiles some Capistrano tasks I've developed and been using for a while. Just to learn how to compile a gem by myself and also how to create a <em>cookbook</em>.

It's also the first time I make public some of my code, and well, it's exciting. Forking, testing, comments, suggestions are of course welcome. I'm willing to listen to everything you might suggest, from changing names of symbols, using other commands, or even changing the license. That's the point of releasing it into the wild :) Thanks in advance.

You can get the gem at <a href="http://www.github.com/jantequera/capistrano-tasks">github.com/jantequera/capistrano-tasks</a>. Where else?

I used <a href="http://openmonkey.com/blog/2010/01/19/making-your-capistrano-recipe-book/">this article</a> (bit outdated, tho) by Tim Riley and the <a href="https://github.com/webficient/capistrano-recipes">Webficient Capistrano Cookbook</a> to learn the how-to. The second link provides a gem/cookbook much more suitable for production use, if you're looking for one. I consider my gem for personal use and learning.
