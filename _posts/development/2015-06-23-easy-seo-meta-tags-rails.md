---
layout: post
title: Easy SEO meta tags in Rails 4
categories:
- development
tags: [cookieshq, blogging, METATAGS, RAILS, SEO, TURBOLINKS]
---

> Having the right SEO meta tags generated for your website is a must have nowadays. Here we show how to get them set easy and fast with the help of the meta-tag gem.

* [Easy SEO meta tags in Rails 4](http://cookieshq.co.uk/posts/easy-seo-metatags-with-rails-4/)
