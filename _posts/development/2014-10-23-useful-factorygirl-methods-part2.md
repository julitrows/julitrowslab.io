---
layout: post
title: Useful FactoryGirl methods part 2
categories:
- development
tags: [cookieshq, blogging, factorygirl, rails, testing, ruby]
---

I've written a second part about Factory Girl for the CookiesHQ blog, here it is:

* [Useful FactoryGirl methods part 2](http://cookieshq.co.uk/posts/useful-factory-girl-methods-part-two/).

Enjoy!
