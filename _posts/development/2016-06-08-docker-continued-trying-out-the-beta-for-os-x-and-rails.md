---
layout: post
title: 'Docker continued: trying out the beta for OS X and Rails'
categories:
- development
tags: [DOCKER, OS X, RAILS]
---

> I was very excited to try out the Docker beta for OS X, and wrote about it:

* [Docker continued: trying out the beta for OS X and Rails](http://cookieshq.co.uk/posts/docker-continued-trying-out-the-beta-for-os-x-and-rails/)

Related: [Common problems when starting with Docker and Rails](http://cookieshq.co.uk/posts/common-problems-when-starting-with-docker-and-rails/)
