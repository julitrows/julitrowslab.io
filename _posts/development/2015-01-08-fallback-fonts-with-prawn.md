---
layout: post
title: Fallback fonts with Prawn
categories:
- development
tags: [cookieshq, blogging, PDF GENERATION, PRAWN, RAILS, RUBY]
---

I've written a tutorial about how to deal with fallback fonts in Prawn, to avoid characters like eastern european accents not being shown.

* [Fallback fonts with Prawn](http://cookieshq.co.uk/posts/fallback-fonts-in-prawn/)

Enjoy the reads!
