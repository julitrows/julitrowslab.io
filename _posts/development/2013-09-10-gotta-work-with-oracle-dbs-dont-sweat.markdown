---
layout: post
title: Gotta work with Oracle DB's? Don't sweat!
categories:
- development
tags:
- how to
- databases
- oracle
- virtual box
- virtualization
status: publish
type: post
published: true
---
I'm currently working on a project that in which Rails is connected to an Oracle database. In order to keep it simple, a workmate in the same project passed along this <a href="http://www.oracle.com/technetwork/database/enterprise-edition/databaseappdev-vm-161299.html">Oracle virtual appliance</a>, that once imported into your VirtualBox, will enable you to use a simple Red Hat Enterprise Linux 5 VM armed with everything you need to use an Oracle DB environment.

You'll just need to configure the VM network adapters to make it visible from your host OS, and use the IP of the VM in your database.yml settings file.

You'll have of course to know how to do basic Oracle DB management tasks, such as creating the database and the tablespaces, but anyhow, you'll be able to use the ODB without problems and keep your system clean :)

<strong>CAVEAT EMPTOR:</strong> having a VM running consumes a lot of RAM and CPU resources. Take into account your system capabilities when doing this, in the end you might prefer to sacrifice the "cleanliness" of your box to get an usable development system.
