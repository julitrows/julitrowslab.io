---
layout: post
title: Comenzando con Rails
categories:
- development
tags:
- how to
- ecosistema
- por donde empezar
- primeros pasos
- ruby on rails
status: publish
type: post
published: true
---
De vez en cuando, alguien aparece con la misma pregunta:

> ¿Qué necesito para poder programar en Rails? ¿Qué recursos me recomendáis?

Como ya he respondido lo mismo más de una vez, y se que volverán las oscuras golondrinas, he decidido hacer un pequeño post explicando por donde empezar, qué libros mirar, etcétera. Todo <em>opinionado</em>, como Rails: todo basado en mi gusto y experiencia personal.

##El ecosistema Rails, cosas que interesa saber:

1. El Sistema Operativo
2. RVM/RBENV
3. Ruby
4. Rails
5. Git
6. IDE
7. Base de datos
8. Testing
9. El Alto Camino del Frontend
10. El Oscuro Camino del SysOps
11. Otros recursos

### 1 El Sistema Operativo

Esto es fácil: OS X o GNU/Linux. Nunca me he aventurado a desarrollar Rails en Windows, y por lo que he leído, es algo que no quiero hacer salvo que me ofrezcan MUCHA pasta por ello. Tanto para desarrollo como para despliegue.

<strong>NOTA:</strong> por preguntas que me han hecho, añado un par de links que pueden ser interesantes: <a href="https://blog.engineyard.com/2012/rails-development-on-windows-seriously">Developing Rails in Windows, seriously</a>, y <a href="http://weblog.rubyonrails.org/2006/5/11/deploying-rails-on-windows-servers/">Deploying Rails to Windows Server</a>.


### 2 RVM y RBENV

Son gestores del entorno para Ruby/Rails y sus gemas (librerías). Cada uno funciona de una forma y sirven para controlar que version de Ruby hay instalada, que librerías y en qué versión están visibles para tal o cual proyecto, etc.

Para despliegues en producción, me gusta Rbenv, no lo conocía hasta hace poco y mola bastante. Para mi máquina local, trabajo con RVM por que fue el primero que conocí y ya es costumbre. Este [post](http://strandcode.com/2013/07/11/ruby-version-manager-rvm-overview-for-rails-newbs/) (en inglés) viene muy bien para enterarse de como funciona RVM.

Llegar a comprender como funcionan y sacarle partido es comprender como se organiza el ecosistema, como interactúan y son dependientes entre sí Rails, Ruby y las gemas que se usen.

* [Rbenv](http://rbenv.org/)
*	[RVM](https://rvm.io/)

### 3 Ruby

El lenguaje en sí. Ruby es a Rails lo que Python a Django o PHP a Laravel. Hay dos formas de enfocar esto: aprender Ruby conforme aprendes Rails, o aprender primero Ruby.

Yo hice lo primero por necesidad, y bueno, se tira para adelante, pero cuando sepas más Ruby, volverás a ver código antiguo y te avergonazarás al ver los pifostios que montabas para resolver cosas que con una sola buena línea de Ruby haces ahora más elegante y rápidamente. Es normal.

A mi me gusta mucho el [Why's (Poignant) guide to Ruby](http://mislav.uniqpath.com/poignant-guide/book/), que por cierto recomienda otros libros que son muy buenos también.

### 4 Rails

El framework. El esqueleto de tu aplicación. El conjunto de herramientas, comandos, estructuras y funcionalidades que te ayudarán a crear sitios web molones en dos patadas (bueno, quizá algo más). Una lista de recursos:

<strong>Rails Tutorial</strong>

[Este](http://ruby.railstutorial.org/) lo hemos hecho casi todos, es básicamente el "Hola mundo" del Rails

<strong>Las guías Rails</strong>

Con esto hemos empezado todos a pegarnos con Rails. Explicaciones, soluciones y consejos básicos y rapidos para empezar a andar, algo así como "The Rails Way in a nutshell". Imprescindibles!

* [Rails Guides](http://guides.rubyonrails.org/v3.2.13/)
* [Rails API](http://api.rubyonrails.org/)

<strong>The Rails Way</strong>

La <strong>BIBLIA</strong> de Rails. [Aqui](http://www.amazon.com/Rails-Edition-Addison-Wesley-Professional-Series/dp/0321601661) te explican como funciona todo y porqué (cosa bastante importante, no todo va a ser "magia").

Ojo, que existen varias versiones: el original para Rails 2, el que salió para Rails 3, y el que ha salido hace poco para Rails 4. El problema de este libro es que algunas partes se te pueden quedar algo obsoletas por ejemplo si estás trabajando en Rails 3.2, ya que el libro cubre Rails 3.0, pero en términos generales, está todo ahí.

### 5 GIT

Es un sistema de control de versiones, que es muy popular y casi está considerado ahora mismo como el estándar de facto. [Aquí](http://git-scm.com/book) está la manteca:

Adicionalmente, en <strong>Github</strong> podrás guardar tus proyectos (o en <strong>Bitbucket</strong> si necesitas que sean privados) y estar en contacto con la comunidad de desarrolladores, creando gemas, ayudando a mejorarlas, y averiguando como funcionan. Colaborar y aprender, los pilares del FLOSS.

Ojo, esta parte es algo fundamental en el desarrollo y aprendizaje (contínuo) de Rails. Cuando estés más pendiente de lo que pasa Github que de lo que pasa en Facebook, estarás en el buen camino :P (de ser un m4m3r70 pr0, pero hey tiene su aquél, ¡en serio!)

### 6 IDE

Hay algunos IDES para Rails funcionando que no he probado. Por lo que sé, la forma más habitual de trabajar es consola de comando, un editor de textos potente (TextMate2, SublimeText2, Vim, Gedit o Emacs) y varios navegadores en los que probar.

Todo lo demás que puedas conseguir con GUI para ayudarte si tienes miedo a la consola no te vendrá mal: clientes gráficos de Git, el JewelryBox para la RVM, SequelPro o PgAdmin para tus bases de datos, etc. Usa lo que necesites, con lo que te sientas más cómodo, y lo que tu máquina pueda correr (algunos de los softwares citados solo existen para OS X).

Personalmente, tiro de <a title="TextMate 2" href="http://macromates.com/" target="_blank">TextMate2</a>, 4-5 terminales abiertos por proyecto (servidor Rails, IRB, SassWatch, RSpec y una "libre"). Uso la app de <a title="Heroku Postgres" href="http://postgresapp.com/" target="_blank">Postgres de Heroku</a> para gestionar el servicio de PostgreSQL, <a title="PgAdmin" href="http://www.pgadmin.org/" target="_blank">PgAdmin</a> o <a title="SequelPro" href="http://www.sequelpro.com/" target="_blank">SequelPro</a> para GUI de la BD. Raras veces uso <a title="JewelryBox" href="http://jewelrybox.unfiniti.com/" target="_blank">JewelryBox</a> y <a title="SourceTree" href="http://www.sourcetreeapp.com/" target="_blank">SourceTree</a>, ya que suelo tirar casi siempre de consola para operaciones de Git y RVM.

### 7 Base de datos

¿PostgreSQL? ¿MySQL? ¿SQLite? Para el día a día, es complicado que tengas que llegar a pegarte con tu base de datos cara a cara, consulta a consulta, picando SQL 'a pelo', salvo que estés trabajando en algo MUY GORDO (¿qué haces leyendo esto, entonces?). Rails ofrece ActiveRecord, una interfaz de consultasa base de datos bastante potente que te facilitará el trabajo (y es legible por humanos!).

Aun así, conocer SQL no te vendrá NUNCA NADA mal (perogrullada). No, en serio, es una de esas cosas básicas que hay que conocer en esto del desarrollo web. Aunque también es cierto que la gran parte de las veces solo te tendrás que pegar con tu SGBD para importar o hacer dumps de la base de datos o crear usuarios :P

### 8 Testing

Rails viene con _TestUnit_ (creo recordar) para tests unitarios. Yo por mi experiencia recomiendo [RSpec](http://betterspecs.org/), [Capybara](https://github.com/jnicklas/capybara) y [Cucumber](http://cukes.info/) para cubrir TODOS los ámbitos de testing, desde unitarios hasta aceptacion y features (hablando ya también de BDD y no sólo TDD). En Rails Tutorial lo explican bastante bien de qué va todo, al menos lo básico.

* [Test Driven Development](http://es.wikipedia.org/wiki/Desarrollo_guiado_por_pruebas)
* [Behaviour Driven Development](http://en.wikipedia.org/wiki/Behavior-driven_development)

### 9 El Alto Camino del Frontend

<strong>CoffeeScript</strong>
A grandes rasgos es una versión de la sintaxis de Javascript que hace que se parezca un poco  a Ruby, para tener código más legible y claro. Una forma más bonita de escribir Javascript, vaya. Pero tiene más, yo sigo aún descubriéndolo :D

De **Javascript y jQuery** pues, qué decir a estas alturas. Llevan funcionando tanto tiempo que resulta manido hablar de ellos :) Si estás leyendo esto ahora, seguro que sabes de qué van, o incluso te manejas con ellos.

### 10 El Oscuro Camino del SysOps

<strong>Capistrano</strong>
Es un sistema de despliegue automatizado para Rails. Facilita la vida porque si te montas bien los scripts en tu máquina de desarrollo, con cuatro comandos de tu consola LOCAL (él solo se conecta por SSH y hace todo) pones la aplicación a funcionar desde cero, teniendo solo el servidor recién contratado.

Instalar las bases de datos, librerias, ruby, rails, gemas, el código, todo con cuatro comandos. El arte está en hacer bien los scripts, claro, y saber como funciona el SO sobre el que vas a desplegar. Por ejemplo no es igual el tratamiento de servicios de sistema en Debian que en Ubuntu, de ahi que tengas que tener nociones de administración de sistemas.

En mi opinión, es una herramienta FUN-DA-MEN-TAL. También he de decir que yo soy más ducho en el camino oscuro del sistemero, pero bueno. Cierto es que permite a gente sin mucha noción de sistemas configurar un servidor de forma rápida y sencilla para tareas básicas, gracias a que hay quien cuelga sus recetas (scripts) en Github. Solo necesitas saber leer y entender qué hacen, para ver si te interesa usarlas.

### 10 Otros recursos

**Otros Tutoriales**
En Udemy tienen un tutorial bastante completo. [Aqui](https://blog.udemy.com/ruby-on-rails-tutorial-learn-from-scratch/)

<strong>Cursos online</strong>
Son muy interesantes y amenos de hacer los de [CodeSchool](http://www.codeschool.com/paths/ruby#starting-rails). Prueba los gratuitos, y si te molan ya vas a los de pago. Como complemento, los de [jQuery](http://www.codeschool.com/paths/javascript#jquery-basics), [Coffeescript](http://www.codeschool.com/courses/coffeescript) y [Sass](http://www.codeschool.com/paths/html-css#sass) ayudan también.

Yo no los he hecho todos (alguno de pago he pillado cuando les da por ponerlos un día gratis). No te van a enseñar nada que no puedas averiguar con los recursos gratuitos y escritos que hay por la red, pero están muy bien presentados y son muy entretenidos de seguir.

<strong>Railscast</strong>

[Videos cortos](http://railscasts.com/) en los que Ryan Bates explica de forma corta, amena y con la consola por delante como hacer cosas con Rails y el uso de gemas (librerías) populares. Esto no es para verlo desde el 1 al ultimo, al menos no después de los básicos. Estos son de consumir conforme necesites info: buscar cuál habla sobre lo que te interesas y te miras el que necesites.

<strong><em>¿Me dejo algo? ¿Crees que este post es mejorable? ¿Quieres rebatirme o aconsejarme? ¿Contratarme? ¡Comenta!</em></strong>
