---
layout: post
title: Upgrading to Roadie 3 for email styling
categories:
- development
tags: [DEVELOPMENT, EMAIL, FRONTEND, GEM, ROADIE]
---

> While working on a Rails 4.2 app, we needed to update Roadie to version 3, and we also updated our transactional email templates for beautiful and responsive styling:

* [Upgrading to Roadie 3 for email styling at CookiesHQ blog](http://cookieshq.co.uk/posts/upgrading-roadie-for-email-styling/)

Related: [Styling emails with Rails and Roadie](http://cookieshq.co.uk/posts/how-to-style-emails-with-rails-and-roadie/)
