---
layout: post
title: Site revamp
categories:
- development
tags: [jekyll, bootstrap, blog, personal]
---

Some months ago a workmate suggested we upgraded some of our pages from Bootstrap version 3.4 to 4.4. We didn't do it in the end, since this came up inside a specific project and we didn't want to fix some other critical pages while doing this upgrade.

Also we had some voices in the team advocating for ditching Bootstrap altogether, so, in the face of several possibilities, inaction took the best of us.

Anyway, while doing this work several differences in the usage of classes in the HTML markup made me curious about Bootstrap 4, so I decided to give it a try on this very site.

This past couple of days I have set this jekyll project to use the latest and greatest from Ruby and Jekyll, removed MaterializeCSS and moved everything to Bootstrap 4. In the meantime, I've changed the colorscheme of the site, as well as lots of copy, the layout and navigation (now you get to see my face when you first enter the site!).

I've kept everything as vanilla Bootstrap as possible, and it's fast to use. Looking at the dev tools I see there's a ton less of CSS than it used to be on previous versions, so that's a win!
