---
layout: post
title: The benefits of using a coworking space as a remote worker
categories:
- development
tags: [cookieshq, blogging, COWORKING, OFFICE, REMOTE, WORK]
---

> As a remote worker, or even as a freelance, you might spend lots of time working from home. A coworking space is a good idea to try out. I recently did so, here's my experience.


* [The benefits of using a coworking space as a remote worker](http://cookieshq.co.uk/posts/benefits-using-coworking-space-remote-worker/)
