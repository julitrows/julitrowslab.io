---
layout: post
title: ElasticSearch on Rails
categories:
- development
tags: [elasticsearch, ruby on rails, cookieshq, mongodb]
---

Yesterday I published an article on the CookiesHQ website, talking about the integration of ElasticSearch on a Ruby on Rails application that uses MongoDB.

[Check it out!](http://cookieshq.co.uk/posts/create-an-elastic-search-spotlight-like-search-on-rails/)