---
layout: post
title: Simple good practices you're probably not following
categories:
- development
tags:
- good practices
- project setup
- ruby on rails
- seeds.rb
status: publish
type: post
published: true
---
This one is simple, you might as well tattoo it all over your face, because it's very likely you're not doing it:

<blockquote>Keep your seeds.rb information up-to-date with your data model, for Pete's sake.</blockquote>

Make the next guy to work in that project's life easier by not having him editing the seeds.rb file as he spends like two hours invoking rake tasks to setup database and getting fugly errors in the terminal one after another.

Be the good guy. You don't know when you're getting to continue other's work in a project, and you that b-word they say about karma.
