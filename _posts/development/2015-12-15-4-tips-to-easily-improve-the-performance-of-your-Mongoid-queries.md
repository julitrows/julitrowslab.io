---
layout: post
title: 4 tips to easily improve the performance of your Mongoid queries
categories:
- development
tags: [cookieshq, blogging, MONGODB, MONGOID, PERFORMANCE, QUERIES, RAILS]
---

> Slow queries? Frightening timeouts? Review your code and see if you can apply these simple yet powerful performance tips! Read it a the CookiesHQ blog:

* [4 tips to easily improve the performance of your Mongoid queries](http://www.cookieshq.co.uk/posts/4-tips-to-easily-improve-the-performance-of-your-Mongoid-queries/)
