---
layout: post
title: We have created 'Hebillas', a suspender Ruby on Rails template companion
categories:
- development
tags: [COOKIESHQ, DEVELOPMENT, RAILS, TOOLING, OPEN SOURCE]
---

> I worked on expanding and updating our Rails template at the CookiesHQ, and the result is an open sourced template companion to thoughtbot's Suspenders.

* [We have created 'Hebillas', a suspender Ruby on Rails template companion](http://cookieshq.co.uk/posts/we-have-created-hebillas-a-suspender-ruby-on-rails-template-companion/)

Related: [CookiesHQ Rails Template](http://cookieshq.co.uk/posts/chookieshq-rails-template/)
