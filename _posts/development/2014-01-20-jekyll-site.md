---
layout: post
title: Julio ❤️ Jekyll
categories:
- development
tags: [jekyll, ruby]
---
I've revamped my website and done it entirely using the lovely [Jekyll](http://jekyllrb.com). Simple to work with and as beautiful as your frontend capabilities go!

The site template was originally created by my great friend [Juan Pablo López](http://www.inefable.net/) for a simple resumé site, and I'm trying to adapt it to cover a whole site. By the way, he's a great UX and frontend freelancer, so you might as well consider to [hire him](http://cv.inefable.net).
