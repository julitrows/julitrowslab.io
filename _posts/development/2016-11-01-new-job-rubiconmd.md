---
layout: post
title: New job at RubiconMD
categories:
- development
tags: [job, remote, ehealth]
---

This week I've started working for [RubiconMD](http://rubiconmd.com), an eHealth company based in New York! I'm really excited since the project is something that I believe helps people in such complicated moments as going to the doctor's.

Also, since the time difference between Seville and New York is 5-6 hours, this will require more self sufficiency when developing new features or fixing bugs, I know it might be a bit hard in the beginning, but I can't wait to give the best of me and get better at what I do.

Also the technologies in use and the technical challenges ahead will help me learn a lot of new things and grow as a dev. And soon, I'll be going to NYC to meet the team!

So, interesting times ahead! Wish me luck!
