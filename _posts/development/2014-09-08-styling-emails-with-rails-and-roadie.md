---
layout: post
title: Styling emails with Rails and Roadie
categories:
- development
tags: [cookieshq, blogging, DEVELOPMENT, EMAIL, FRONTEND, GEM, ROADIE]
---

Here's the tutorial about Roadie I wrote for the CookiesHQ blog, that was featured on [Ruby Weekly #212](http://rubyweekly.com/issues/212):

* [Styling emails with Rails and Roadie](http://cookieshq.co.uk/posts/how-to-style-emails-with-rails-and-roadie/).

Hope it helps!
