---
layout: post
title: ! 'Agilismo: lugares comunes para recién llegados'
categories:
- development
tags:
- talks
- adaptacion
- agile
- agilismo
- charlas
- formación
- scrum
status: publish
type: post
published: true
---
Esta pasada semana tuve la oportunidad de participar junto con la gente de <a href="http://www.salumedia.com/">Salumedia</a> en una muy buena sesión de formación sobre la gestión de equipos y proyectos desde el punto de vista del Scrum Master, impartida por <a href="http://www.joseiogr.com/">José Ignacio Ortiz de Galisteo</a>.

Fue una sesión muy abierta y dinámica, en la que los que ya habíamos trabajado con Scrum alguna vez pudimos hablar de nuestra experiencia y hacer ver mejor a los "no iniciados" cómo se hacen algunas cosas, mientras que estos últimos pudieron preguntar todas sus dudas (e insistir en algunas de ellas) sin límite. Incluso se consiguió no lanzar la "alerta de trauma" ni una sola vez :)

Durante la sesión, y posteriormente, reflexionando y recordando el ciclo "<a href="http://ingenius.informatica.us.es/index.php/es/noticias/ingenius/460-lunes-agiles">Lunes ágiles</a>" sobre agilismo que <a href="http://davidjguru.com/">David JGurú</a>, <a href="http://jvrbaena.github.io/">Javier Baena</a> y yo dimos en la Universidad de Sevilla a finales de mayo, y en el que precisamente yo cubrí el <a href="https://speakerdeck.com/jantequera/agilismo-desde-las-profundidades-del-averno">rol de Scrum Master</a>, me he dado cuenta de que al introducir Scrum a un equipo nuevo siempre acaban apareciendo los mismos lugares comunes.

<h2>La ¿panacea? de la estimación en equipo:</h2>

Pese a parece algo liosa en principio (¿planning poker? ¿sucesión de Fibonacci?), es bien recibida. La gente está muy cansada de estimaciones irreales que llegan desde arriba, hechas por gente que generalmente ha oído campanas sobre el desarrollo, pero realmente no sabe dónde. Por tanto, es normal que esta forma de "empoderar" al equipo de desarrollo ilusione tanto. Pese a todo, este tipo de estimaciones tienen riesgos y dificultades, a saber

<strong>- La sobreestimación y la subestimación</strong>. Mientras que la primera puede ser fruto de la inexperiencia (o de un realismo extremo) y las sucesivas iteraciones deberían ir eliminando esta práctica, lo segundo creo que suele venir por influencias externas ("no se puede tardar tanto en tener esto") y puede ser altamente tóxica para el equipo. Es algo a evitar SIEMPRE. Una estimación bajo presión que acabe con mucha "subestimación" acabará pasando factura anímica (porque el trabajo es el que es y se tarde lo que se tiene que tardar), por no hablar de que malvender las horas de trabajo es nocivo para el negocio de por sí.

<strong>- Compromiso versus contrato.</strong> El equipo se tomará las estimaciones como el compromiso que asumen: "en tanto tiempo tendremos tantas cosas". El problema llega cuando desde el PO/cliente se entiende que una estimación es algo absoluto y escrito en piedra, y que jamás se va a tardar más de lo que se dice porque nunca va a haber imprevistos. Ahi es cuando entra el dolor. Se ha de comprender que hay cosas que hasta que no se está hasta las rodillas en la faena, no aparecen.

<strong>- La obsesión con las horas.</strong> Esto suele darse entre la gente de las capas "de gestión" que llegan de nuevas. El concepto de puntos de esfuerzo les parece demasiado abstracto, no hacen más que preguntar por la equivalencia en horas. Lo importante es aprender qué implican y significan (y no "cuanto") para tu equipo tres, cinco, ocho puntos de esfuerzo, y la conversión a horas vendrá sola, gracias al proceso subyacente de auto-conocimiento del equipo que conlleva el adoptar scrum.

<h2>Como hacer todo esto de cara al cliente</h2>

Esto es más de cara a un PO/comercial que un SM. Por esto es recomendable <a href="http://www.slideshare.net/davidjguru/agilidad-desde-las-profundidades-del-averno">enlazar aquí</a> la parte que dió David al respecto en el ciclo de agilismo.

Suele ser habitual el "¿como le vendo al cliente esto si no le puedo decir que van a ser 2 meses, tres semanas y cuatro dias JUSTOS de desarrollo?", "¿como les hablo de sprints? ¿de que en dos semanas van a tener algo que se va a ir transformando en lo que quieren?", "¿como hago para que cada sprint suponga aumentar el valor del producto, en lugar de pivotar completamente porque el cliente no tiene claro lo que quiere?".

Todo esto es bastante complicado, concedido, porque requiere en mayor o menor medida reeducar al cliente. Esto es más labor de un PO/comercial si este pertenece a tu organización, en lugar de a la del cliente. Tendréis que aprender a vender proyectos de otra forma (juntos, el PO es parte del equipo, más que un comercial al que solo se le ve de higos a brevas por la ofi y con una bolsa cargadita de marrones). Aprovechad el factor iterativo y el de "más resultados, más pronto", e involucrad al cliente más en el proceso. Abandonad la mentalidad de "desarrollos caja negra", y trabajad más codo con codo con el cliente a base de implicarle.

Si toda vuestra organización va a ser ágil, pero vuestros clientes no van a cambiar con vosotros, aquí sólo va a haber DOLOR. Llegará ese momento en el que habrá que decir "no somos los desarrolladores que buscas". Y sí, perder un cliente es doloroso, pero más doloroso es tener a un equipo encabronado en un ambiente tóxico, un cliente encabronado, y estar tú en medio.

Y si el PO que tenéis es del lado del cliente, y no entiende esto, entonces necesitáis quitaros a esa persona de encima, porque no ha entendido nada de nada.

<h2><strong>Conclusión</strong></h2>

En definitiva, adoptar Scrum no es cosa de un día. Requiere un aprendizaje, un sacrificio y tiempo de adaptación para todos los roles implicados. Son necesarios paciencia, comprensión y apertura mental.

Pero merece la pena. Porque implica una cultura organizacional diferente. Porque da poder al equipo de desarrollo, y porque nos reeduca a todos, poniéndonos en el sagrado camino de ser felices mientras trabajamos (que al final es lo más importante).

Y porque bueno, cuando lo echas a andar, <a href="http://www.slideshare.net/fjavierbaena/agile-funciona">Scrum funciona</a> y todo.
