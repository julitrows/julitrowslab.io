---
layout: post
title: ! '#OpenSpaceSevilla'
categories:
- development
tags: [open space sevilla, openspacesevilla, events, talks]
status: publish
type: post
published: true
---
> **DISCLAIMER**: being a post about a local event, I've decided to write it in Spanish. This should help me to finish it fast and publish it while the impressions from the event are still fresh in our heads. If you still want an English version, help yourself with the Google Translation apps.

Este fin de semana acudí a mi primer _sarao_ profesional, el <a href="http://openspacesevilla.com/">#OpenSpaceSevilla</a>. Lo primero, como debe ser siempre, es <strong>agradecer el esfuerzo</strong>, dedicación y atención a los organizadores. No faltó de nada, y gracias a ellos y a los asistentes hubo muy buen ambiente en todo momento :)

### Web Security por Pedro Laguna

Pese a lo improvisado, es un tema que nunca falla. Siempre hay anéctodas, prácticas y discusiones que salen a la palestra, en especial sobre la valoración que hace el cliente sobre ésto. ¿Debe la seguridad ser un valor añadido, o una <em>core feature</em> del software/servicio que ofrecemos? Personalmente soy más partidario de la segunda opción: la seguridad debe estar ahí, la pida el cliente o no. Esto puede requerirnos más esfuerzo como desarrolladores, o jugárnosla al ser más caros, pero creo que usando buenas metodologías y frameworks de desarrollo el impacto no debería ser tan dramático.

Hablando de frameworks, se hizo hincapié en una cuestión interesante: ¿confiamos demasiado en los frameworks que usamos? ¿los auditamos? ¿o es mejor encargarnos de todo nosotros mismos? Creo que para sustentar mi opinión en el apartado anterior, es obvio que soy partidario de delegar en los frameworks que usamos. La comunidad de usuarios y/o desarrolladores se encargará de mantener seguro el framework, simplemente es cuestión de estar al día de versiones y parches (esto ultimo es muy importante, y no siempre se sigue).


### Aplicaciones tipo revista para iOS y Android por Javier Alonso

Bastante interesante por la relativa facilidad para crear este tipo de aplicaciones, me ha dado una idea para algún proyecto personal :) En cuanto a lo negativo: lo de Android era más un "reclamo publicitario" que otra cosa :P

También me gustaría comentar que se preguntó sobre como subir aplicaciones al Market de Apple desde un "Hackintosh", y la verdad, me parece un absurdo. Si quieres jugar a ser dev de iOS porque mola Apple, sigues las reglas de Apple, pasas por el aro, pagas y te quitas de tonterías. Ah, ¿que es caro? Pues desarrolla para Android. Analogía: si quieres ser guay y moderno y fardar de zapatillas Converse, te compras unas puñeteras Converse <strong>originales</strong>. Si no te quieres gastar la pasta, te compras la Beppi de imitación cutres de la tienda de tu barrio, PERO NO LES VAYAS A PEGAR EL ICONO DE LAS CONVERSE, MIARMA, NO ME SEAS UN <strong>CUTRE</strong>.


### Garajes en Andalucía, propuesta por José Manuel Beas

Esta charla comenzó muy muy bien, con muy buenas ideas y una muy buena dirección, hasta que fué acaparada por uno de los asistentes y desviada de la dirección original hacia un lamento continuo, un lloro y un solo ver lo negativo. No se consiguieron abordar ideas como la madurez del mercado en la región (se impuso la idea de que hay que irse a buscar el cliente en otro sitio, aun estando situados aquí), ni, -porque no-, la propia madurez de los profesionales del sector que empiezan su carrera profesional en Andalucía.

O lo que es lo mismo: la gente que va a dedicarse a esto, ¿sale imbuida del "espiritu garajero a.k.a. emprendedor" de donde quiera que ha sido instruido? La respuesta, en mi opinión, viene a ser que no, y creo que es algo que viene directamente de los métodos de formación y evaluación, desde mi experiencia, en la universidad. Luego ya está lo de que cada uno debe ser consciente de sí mismo y su entorno, pero es otra faceta más del mismo problema, creo yo.

Al final se atisbó algo de luz, al hablar de la dificultad de hacer comunidad, en concreto en Sevilla. La dificultad de montar eventos y charlas (que nos lo digan a los Wombytes) y que acuda el personal. De nuevo me remito a lo anterior. Me hubiera gustado que se ahondara más en ciertas recomendaciones que iban surgiendo para dar visibilidad a eventos y hacer algo de piña y masa, pero el tiempo no lo permitió.


### Yo te la mido (la campaña), por David JGurú y Rubén Teijeiro

Me gustó mucho el planteamiento temático (en torno al surf) de la presentación, la idea, la motivación, toda la explicación del por qué y el como, el equipo, y el insistir y volver a insistir en opensource y el "lo damos gratis y libre", una auténtica <em>rara avis</em> en éste evento.

Si debo decir que la parte en la que se muestra código me pareció algo floja, perdiendo la atención de muchos asistentes. Creo que es simplemente cuestión de seguir desarrollando la demo y poder mostrar más chicha en acción.


### JS's varios para hacer de todo, propuesta por Javier Alonso

Dada mi inexperiencia con las tecnologías tratadas (no me he pegado de verdad con node.js aún), éste fue el coloquio en el que estuve más perdido, aunque creo que sí que fue el más "open" de todos. Cada uno fué contando su experiencia y sus ideas respecto a los diferentes "js" tratados, consejos, mitos desmontados, referencias, documentaciones, un auténtico ejercicio de compartir conocimientos.


### Historias de usuario, por José Manuel Beas

Sin lugar a dudas, la mejor ponencia de las que asistí. Hace poco que sigo a Beas en Twitter, y he leído algo de su blog, y la verdad es que sus intervenciones en el resto de charlas me gustaron mucho, pero me resultó impresionante verle "en acción" llevando realmente la batuta en una ponencia, y hablando de lo que más sabe. En cuanto a las historias de usuario, es algo con lo que tengo contacto en mi trabajo (llevo cuatro meses trabajando en un equipo ágil), y cada vez creo más firmemente que el agilismo es el camino para hacer las cosas bien, y hacerlas a gusto.

Obviamente las inquietudes sobre el mapeo de requisitos no funcionales, el catálogo de requisitos y demás parafernalia proveniente de la metodología tradicional es algo que a todos nos preocupa/ha preocupado en algún momento, pero creo que en muchos casos son cosas que por experiencia van saliendo solas. Me gustó mucho el hincapié que se hizo en el tablón físico, en el "yo soy el que mueve la tarjeta", el "veo el tablón y de un vistazo tengo una impresión de como va la cosa", me sentí muy identificado, me gusta tener las cosas apuntadas en papel, y por ejemplo, tachar una tarea realizada en un papel resulta infinitamente más gratificante que marcar un check en una aplicación móvil. Creo que trasladar a lo físico ciertos aspectos de nuestro trabajo, por muy IT que seamos, es esencial, a muchísimos niveles.

Y hasta aquí en cuanto al contenido de las charlas (me salté la última y la retro, soy mala persona).

En cuanto a <strong>la experiencia, tremendamente positiva</strong>. Al ser la primera vez, considero en global todo, y repetiría sin pensármelo. Quizá en cuanto a contenidos y charlas, podría mejorarse tal o cual cosa (contenido más técnico, o no; más control sobre determinadas actitudes), pero creo que simplemente es cuestión de estar ahí en la próxima ocasión, y aportar/proponer yo mismo algo, callar menos y participar más (aunque soy de natural observador, qué voy a hacer), y adaptar así el evento más a mis gustos y preferencias. Es así de simple y así de bonito. Lo mismo si fuera la cuarta o quinta vez que acudo a un sarao de estos no quizá estaría tan contento, pero la verdad es que me siento muy satisfecho.

Y la verdad, por 12€ un día y medio de conocer a personas, profesionales y absorber tanto conocimiento, los amortizas y amortizarás más y mejor que cualquier Máster de tres al cuarto de esos que hay por ahí.
