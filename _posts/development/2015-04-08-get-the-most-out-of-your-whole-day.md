---
layout: post
title: Getting the most out of your WHOLE day
categories:
- development
tags: [cookieshq, blogging, PERSONAL, PRODUCTIVITY, TIME MANAGEMENT]
---

Going to bed everyday with the feeling of having accomplished something else than work is really hard. I talk about it on my latest blog post for CookiesHQ:

* [Getting the most out of your WHOLE day](http://cookieshq.co.uk/posts/getting-the-most-out-of-your-whole-day/)
