---
layout: post
title: Interesting gems for better code quality
categories:
- development
tags: [gems, tools, code quality]
---

While reading through the last 8 issues of [Ruby Weekly](http://rubyweekly.com/) I had unread in my inbox, I've found and tried out some gems that seem interesting and might bring extra input/advice in order to achieve better code style and quality, of which [I might be a bit obsessed over](https://www.cookieshq.co.uk/posts/code-cleanliness-and-style-with-linters).

* [Rails Best Practices](https://github.com/railsbp/rails_best_practices): after being run on your project root, it will produce an html file with a list of faults found in your code, according to the items detailed [here](http://rails-bestpractices.com/).
* [RubyCritic](https://github.com/whitesmith/rubycritic): runs Reek, flay and flog, to produce a site with metrics, graphs and info similar to what you could find on CodeClimate. Really nice. I always have Rubocop integrated with Atom, my $EDITOR of choice, and could do the same with Reek, but the ouput is a bit overwhelming with this one, so it's better to have it done in a nice site living on the project's tmp folder.
* [Bundler-Audit](https://github.com/rubysec/bundler-audit): this one checks for vulnerabilities in the gems you've got on your Gemfile and the sources you use on it.
* [Brakeman](https://github.com/presidentbeef/brakeman): this one checks for potential security breaches in your code.

There are also some of them I'd like to try sometime, but need quite more configuration and work:

* [Scientist!](https://github.com/github/scientist) and [Suture](https://github.com/testdouble/suture) are tools for safely refactoring code. We all know the 'cover with test, then change' path, but these two might be useful when you're working on critical parts of the code and you want to be extra-super-sure that everything is all right.
* [Coverband](https://github.com/danmayer/coverband) is a tool you need run on production, and it measures code usage. This is extremely useful on large production apps to detect conditional paths or methods are never reached, which might allow you to just delete code or rework it.

Any other gem you find useful and want to reccomend? Tell me on [twitter](https://twitter.com/julitrows)!
