---
layout: post
title: CSS basics - positioning
categories:
- development
tags: [cookieshq, blogging, CSS, DEVELOPMENT, FRONTEND]
---

As I had to review what I knew about css positioning, I thought about writing a post about the basics:

* [CSS Basics: Positioning](http://cookieshq.co.uk/posts/css-basics-positioning/)

Enjoy the read!
