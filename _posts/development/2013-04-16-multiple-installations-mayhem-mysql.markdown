---
layout: post
title: ! 'Multiple Installations Mayhem: MySQL'
categories:
- development
tags:
- how to
- brew
- gem
- mysql
- MysqlCompat::MysqlRes
- rails
status: publish
type: post
published: true
---
While trying to build and install PHP 5.4 in a OS X 10.7.5 in order to test Laravel, I found myself updating several things with Homebrew. It seems that one of the things updated was my brew installation of MySQL.

The thing is that later, I tried to boot the Rails console for a project and it puked this fearsome line:

    ~/.rvm/gems/<gemset>/gems/activesupport-2.3.17/lib/active_support/dependencies.rb:466:in
        `load_missing_constant':NameError: uninitialized constant MysqlCompat::MysqlRes

As delicate as installing MySQL in Mac OS X and getting it to work is, I knew this was a bad omen. Yes. I couldn't make any query using the rails console.

So, after checking the Internet, the first probable cause is that after changing the MySQL installation you would need to rebuild the mysql gem. So, after checking 'which MySQL' was in use, I reinstalled the gem with some options:

    env ARCHFLAGS="-arch x86_64" \
    gem install mysql -v=2.8.1 --with-mysql-config=/usr/local/mysql/bin/mysql_config

Everything went smoothly, but I had the same error when firing up the console. Damn. The next thing I found about it was about the ``libmysqlclient`` not being installed or found. But ``locate libmysqlclient`` gave me three different paths... so it had to be about the library not being visible. After researching, the library is usually looked for in the ``$DYLD_LIBRARY_PATH``. Set the proper path in your ``.bash_profile`` (something along ``/user/local/mysql/lib``) and you'll get going.
