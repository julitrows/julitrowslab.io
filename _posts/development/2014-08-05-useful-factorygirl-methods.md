---
layout: post
title: Useful FactoryGirl methods
categories:
- development
tags: [cookieshq, blogging, factorygirl, rails, testing, ruby]
---

There is life beyond FactoryGirl.create()!!

I've written about Factory Girl for the CookiesHQ blog, here it is:

* [Useful FactoryGirl methods](http://cookieshq.co.uk/posts/useful-factory-girl-methods/).

It was featured at Ruby Weekly [#209](http://rubyweekly.com/issues/209).

Thanks!
