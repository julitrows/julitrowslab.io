---
layout: post
title: Nested forms on steroids with Cocoon
categories:
- development
tags: [cookieshq, blogging, COCOON, DEVELOPMENT, GEM, NESTED FORMS, RAILS, RUBY]
---

We use Coocoon quite a lot on our rails projects for nested forms. It is very powerful and flexible, here's a post with some examples:

* [Nested forms on steroids with Cocoon](http://cookieshq.co.uk/posts/nested-forms-on-steroids-with-cocoon)

Enjoy the read!
