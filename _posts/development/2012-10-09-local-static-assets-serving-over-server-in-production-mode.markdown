---
layout: post
title: Local static assets serving over server in production mode.
categories:
- development
tags: [assets, production, ruby on rails, webrick]
status: publish
type: post
published: true
---
When debugging and testing Rails 3 apps locally, you might eventually need to boot Webrick (or whatever server you use) in "production mode"

    rails server -u -e production

Then when you get to -usually- localhost:3000, you might find that no images, stylesheets or javascripts are being served, and you're getting lots of routing errors on the server output.

Don't panic! This happens because Rails in production mode assumes that your web server (Apache, nginx, etc) will manage serving the assets.

This can be fixed by changing the <strong>32</strong>nd line in your ``config/environments/production.rb`` file:

{% highlight ruby %}
  config.serve_static_assets = false
{% endhighlight %}

From ``false`` to ``true``.

And that's it.
