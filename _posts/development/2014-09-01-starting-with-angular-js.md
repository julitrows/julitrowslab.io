---
layout: post
title: Starting with AngularJS
categories:
- development
tags: [cookieshq, blogging, ANGULAR, DEVELOPMENT, FRAMEWORK, FRONTEND, JAVASCRIPT]
---

First steps with one of the strongest JS frameworks today

I've written about some resources to start learning AngularJS for the CookiesHQ blog, here it is:

* [Starting with AngularJS](http://cookieshq.co.uk/posts/angularjs/).

Enjoy!
