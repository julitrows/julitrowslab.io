---
layout: post
title: Switching from embedded documents to relationships with Mongoid
categories:
- development
tags: [cookieshq, blogging, MONGODB, MONGOID, RAILS]
---

> Lately we've had to change how several documents on the MongoDB database of a project were related to each other. Read the how to at the CookiesHQ blog:

* [Switching from embedded documents to relationships with Mongoid](http://www.cookieshq.co.uk/posts/switching-relationships-with-mongoid/)
