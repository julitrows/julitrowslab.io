---
layout: post
title: About remote pair programming
categories:
- development
tags: [cookieshq, remote work, pair programming, shameless plug]
---
As a member of the [CookiesHQ](http://www.cookieshq.co.uk) team, I've got the right and duty to write on the company's blog. My [first post](http://cookieshq.co.uk/posts/adopting-remote-pairing/) is about how we have tackled pair programming when a member of the team is in Seville and the other one is in Bristol. Check it out!

PS: how does one get to be _good_ at Vim?