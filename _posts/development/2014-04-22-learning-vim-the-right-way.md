---
layout: post
title: Learning vim the right way
categories:
- development
tags: [cookieshq, vim, skills, development]
---

Several times I've tried to get to know Vim better: understand it, use it, become fluent at it and productive. It has not been easy.

But on my recent visit to Bristol to meet the CookiesHQ team, Nicolas Alpi gave us a crash course and introduced us to his Vim Configuration Directory, and although I am still veeeery far from being a Vim-guru, I think I'm set on the right path, which is definitely a step forward.

You can read about getting started in Vim and our CookiesHQ Vim Config on [the post I've just published at the CookiesHQ blog](http://cookieshq.co.uk/posts/first-steps-in-vim/).
