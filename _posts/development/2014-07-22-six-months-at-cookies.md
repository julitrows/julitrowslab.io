---
layout: post
title: Six months at CookiesHQ
categories:
- development
tags: [cookieshq, development, retrospective]
---

This month I've made six at the Cookies HQ, woohoo!!!

In [this blog post at the CookiesHQ](http://cookieshq.co.uk/posts/6_months_at_cookieshq/), I write about how my revision was: my goals, how I achieved them, and the future. Enjoy!
