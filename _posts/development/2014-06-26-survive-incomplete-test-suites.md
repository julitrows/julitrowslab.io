---
layout: post
title: Survive incomplete tests suites
categories:
- development
tags: [cookieshq, testing, ruby on rails, development]
---

Right now I am working on a legacy project for The CookiesHQ, and one of the things I struggle more with every day is an incomplete test suite, that also follows a different way of testing from what I am used to, and on top of that, uses Minitest (yuck!) instead of RSpec.

In [this blog post at the CookiesHQ](http://cookieshq.co.uk/posts/dealing-with-legacy-projects/), I write about how to deal with situations like this. Enjoy!
