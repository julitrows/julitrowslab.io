---
layout: post
title: Permisos de escritura de la AEAT en OS X y GNU/Linux
categories:
- development
tags: [autonomos, gestiones, hacienda, OS X, linux]
---
> **DISCLAIMER:** This post is in Spanish as its potential target are Spanish freelancers struggling with the Electronic Administration. Feel free to use Google Translator to know what the fuss is about.

Algunas declaraciones de la AEAT, creo que las que se ahcen a través de aplicaciones instaladas en el disco duro tipo P.A.D.R.E., requieren una carpeta llamada ``aeat`` en la carepeta ``/Usuarios/fulanito`` con los siguientes permisos:

* Usuario: Leer y escribir
* Staff: Sólo leer
* **Everyone: Leer y escribir** <-- este hay que ponerlo así, por defecto está en "Sólo leer"

Pero otras gestiones, como por ejemplo el modelo ``037`` que he tenido que hacer hoy mismo, requieren que la carpeta ``aeat`` exista en el **DIRECTORIO RAIZ DEL SISTEMA**, es decir, para poder hacer todas las gestiones (o al menos a las que yo me he enfrentado por ahora), necesitaremos estas dos carpetas en nuestro sistema:

    Macintosh HD/aeat
    Macintosh HD/Usuarios/fulanito/aeat

O hablando en términos de consola:

    /aeat
    /Users/fulanito/aeat <-- cambia *Users* por *home* si estás en GNU/Linux

Los permisos con ``chmod`` creo que serían ``757``, es decir ``rwxr-xrwx``

Francamente, no sé como de seguro es tener una carpeta con permisos de escritura para _todo dios_ en el directorio raíz del sistema, pero tener que dar permisos de escritura en el directorio raíz a una aplicación que se ejecuta en el espacio de aplicaciones del usuario me huele a chapuza gorda. Estaría bien que unificaran los criterios de escritura y **todo** fuera a parar a la carpeta personal del usuario.

Lo más absurdo de todo es que he averiguado el error de escritura en el raíz a base de mirar el log que el applet de la AEAT escribe en... tachán tachán... la carpeta ``aeat`` de mi espacio de usuario, sin ningún tipo de problema. Que alguien me lo explique.