---
layout: post
title: Getting to know the Cookies
categories:
- development
tags: [cookieshq, remote working, team management]
---

After two months working with CookiesHQ, this March they decided to fly me over to Bristol, know the team and work from their office for a week. Working remotely is a challenge, and also is knowing each other in person!

We did a couple of blogposts recollecting our views in the experience. [Here is the 'company view' of the visit](http://cookieshq.co.uk/posts/remote-working-bringing-whole-team-together/), by the company director Nathalie Alpi, and [this is how I experienced it](http://cookieshq.co.uk/posts/remote-working-getting-know-team/).

All in all, it was a great experience, and if you work remotely or employ remote workers, you should try to do something similar!
