---
layout: post
title: Clean unneeded dependencies on Homebrew
categories:
- development
tags: [mac, os x, homebrew]
---
Let's say you've uninstalled a formula with Hombrebrew that it's no longer needed. Homebrew won't clean the associated dependencies that are no longer needed. You can do this by running:

`$ brew rm $(join <(brew leaves) <(brew deps <FORMULA>))`

This calls for the formula dependencies and does a join with the installed packages that are not dependencies of other packages, and uninstall the resulting list of packages.

**CAVEAT EMPTOR:** This method uninstalled Qt from my computer, and I needed it for Capybara. This happened because brew only takes into account dependencies of programs installed with brew. Any dependency from outside brew is ignored.