---
layout: post
title: A tiny guide to remotely working while traveling
categories:
- development
tags: [cookieshq, blogging, ORGANIZATION, REMOTE, TRAVELS, WORK]
---

> Last summer I had the opportunity to travel around and mix some time of vacation and remote work. Here are some of my thoughts about being a remote worker on the go. Read it a the CookiesHQ blog:

* [A tiny guide to remotely working while traveling](http://cookieshq.co.uk/posts/a-tiny-guide-to-remotely-working-while-traveling/)
