---
layout: post
title: Introducing the CookiesHQ Rails application template
categories:
- development
tags: [cookieshq, OPEN SOURCE, RAILS, TEMPLATE]
---

The CookiesHQ has released a Rails application template as OSS:

* [Introducing the CookiesHQ Rails application template](http://cookieshq.co.uk/posts/chookieshq-rails-template/)

Check it out!
