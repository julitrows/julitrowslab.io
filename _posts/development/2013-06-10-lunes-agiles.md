---
layout: post
title: Agilismo meets La Vaquilla
categories:
- development
tags: [agile, charlas, scrum, la vaquilla, bonobo squad]
---

Hace poco tuve el placer de impartir junto a mis amigos y "camaradas del metal" [David "JGurú" Rodríguez](http://davidjguru.wordpress.com/) y [Javier "Javo" Baena](http://jvrbaena.github.io/), con los que formo el tridente denominado "Bonobo Squad" unas charlas sobre agilismo y Scrum en la Escuela de Ingeniería Informática de Sevilla.

Dividimos la charla en tres partes:

* El papel del Product Owner - David JGurú
* El papel del Scrum Master - Julio Antequera
* Caso de éxito de aplicacion de agilismo y Scrum: SocialBro - Javier Baena

La presentación completa está disponible en [speakerdeck](https://speakerdeck.com/jantequera/ciclo-lunes-agiles).

Disfrutadla.
