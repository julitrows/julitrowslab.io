---
layout: post
title: Introducing Capistrano
categories:
- development
tags: [talks, capistrano, database, dumps, log, recipes, ruby on rails]
status: publish
type: post
published: true
meta:
  _wpas_done_linkedin: '1'
  publicize_results: a:1:{s:7:"twitter";a:1:{i:361122451;a:2:{s:7:"user_id";s:9:"julitrows";s:7:"post_id";s:18:"172991869406556160";}}}
  _wpas_done_twitter: '1'
  tagazine-media: a:7:{s:7:"primary";s:0:"";s:6:"images";a:0:{}s:6:"videos";a:0:{}s:11:"image_count";s:1:"0";s:6:"author";s:8:"26452654";s:7:"blog_id";s:8:"32926790";s:9:"mod_stamp";s:19:"2012-02-25
    19:12:07";}
---
At <a href="http://wombytes.es/">my job</a>, we decided that twice a month, a member of the team should host a talk in which he would speak about something he could teach to the others. When we spoke about this, I had just succesfully deployed my first Rails application and I kinda liked it (bear with me, I was a C++ systems dev kid for 2+ years, it was a brave new world for me), so I suggested the possibility of giving a talk on <a href="https://github.com/capistrano/capistrano">Capistrano</a>, the Rails default-ish option for deploying automation.

I prepared a neat presentation available <a href="http://julitrows.files.wordpress.com/2012/02/introduccic3b3n-a-capistrano_jantequera.pdf">here</a> (Spanish only). Its most important part is based in a live demo, so I'm sorry if you find that it doesn't cover essential aspects like tasks syntax and such, because they were covered in the hands-on. In exchange, I offer you some simple recipes I made as examples, or by demand of a fellow dev at Wombytes. They aren't intended as showcase of my prowess at reciping, but only as simple examples of what namespaces and tasks are about. In fact, they're the composition of fragments and ideas picked from all around the internets.

This one allows you to see the production log as it grows, without the fuss of having to open a new terminal get into the machine via ssh and navigate to the log folder. Just an elegant <em>cap production log:tail</em> and there you go! In this <a href="https://github.com/webficient/capistrano-recipes">github repo</a> there's a similar one, that executes tail on every log present (pre, production, whatever) instead of just one. The addition of the INT signal treatment was shamelessly taken from <a href="http://stackoverflow.com/questions/5218902/tail-production-log-with-capistrano-how-to-stop-it">this post</a> from the almighty Stackoverflow.

{% highlight ruby %}
  namespace :log do
    desc "Tail production log file"
    task :tail, :roles => :app do
      run "tail -f #{shared_path}/log/production.log" do |channel, stream, data|
        # this catches the INT signal and avoids the ugly capistrano error on CTRL+C
        trap("INT") { puts 'Interupted'; exit 0; }
        # this is to have a cleaner output
        puts "#{channel[:domain]}: #{data}"
        break if stream == :err
      end
    end
  end
{% endhighlight %}

The next set of recipes make use of your regular recipe to make dumps (namely 'backup') of the production database, and need a couple of extra things. Basically what they do is put some order in your dumps folder, and download new dumps you create easily via scp. I don't like the fact that they have too much folder information hardcoded on them, but it's just a matter of refinement and good variable usage.

{% highlight ruby %}
  #put this with your variable declarations
  #the LOCAL folder where you want to store database dumps
  set :projects_folder, 'projects'

  #put this with the rest of your hooks
  before "db:getnewdump", "db:tidydumps"

  # THESE TASKS GO INTO YOUR namespace :db
  desc "Moves old backups to old folder"
  task :tidydumps do
    puts 'Moving old backups to the old folder'
    run "mkdir -p #{shared_path}/db/backups/old"
    run "mv #{shared_path}/db/backups/*.gzip #{shared_path}/db/backups/old"
  end

  desc "Downloads the las db dump"
  task :getlastdump do
    puts "Downloading latest dump to $HOME/#{projects_folder}/database_dumps/#{application}/"
    system "mkdir -p $HOME/#{projects_folder}/database_dumps/#{application}/"
    system "scp -P #{port} #{user}@#{domain}:/#{shared_path}/db/backups/*.gzip
            $HOME/#{projects_folder}/database_dumps/#{application}/"
  end

  desc "Creates a dump and downloads it"
  task :getnewdump do
    db.backup
    db.getlastdump
  end

  desc "Downloads ALL THE DUMPS!!"
  task :getalldumps do
    puts "Downloading dumps to $HOME/#{projects_folder}/database_dumps/#{application}/"
    system "mkdir -p $HOME/#{projects_folder}/database_dumps/#{application}/"
    system "scp -r -P #{port} #{user}@#{domain}:/#{shared_path}/db/backups/*
            $HOME/#{projects_folder}/database_dumps/#{application}/"
  end
{% endhighlight %}

Anyway, the talk went well, as only the devs were present[1] and I felt more comfortable. Some team members learnt something they didn't know about Capistrano (yay for me!), and some others with more experience explained some things I didn't have understood too well to me (yay for me again!).  There's a chance that I will be giving this talk sometime again in the future, so those lucky enought to see me then will get a much better and expanded experience.

This talk gave me the chance to have a conversation with <a href="http://www.google.es/webhp?q=juanjeojeda" target="_blank">Juanje Ojeda</a>, who kindly pointed me in the direction of some nifty <a href="http://www.opscode.com/chef/">Chef</a> (which is mentioned at the end of the presentation) resources, so I can delve deeper into the devops lore. I guess you'll be reading about my findings here in the short future. Thanks, mate :)

That's the best of it: get the satisfaction of teaching a little bit and learn a lot in the process.

[1] Having talks on thursdays at 18:30 in a god forsaken place faraway from downtown doesn't precisely attract many peeps, no matter how interesting the talk seems to be.
