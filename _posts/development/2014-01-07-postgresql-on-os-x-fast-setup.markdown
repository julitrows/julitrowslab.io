---
layout: post
title: PostgreSQL on OS X fast setup
categories:
- development
tags:
- databases
- heroku
- mac
- os x
- postgresql
- rails
- ruby on rails
- setup
status: publish
type: post
published: true
---
[Get the Heroku Postgres.app](http://postgresapp.com)

Add this line to your bash_profile:

<pre>export PGHOST=localhost</pre>

This way you can avoid specifying the host ("-h localhost") in each instruction in the shell, or having to add it to the development ("host: localhost") section in your database.yml (if you're developing with Rails).
