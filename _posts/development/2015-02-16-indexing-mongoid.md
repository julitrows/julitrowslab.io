---
layout: post
title: Indexing with Mongoid
categories:
- development
tags: [cookieshq, blogging, DATABASE, DEVELOPMENT, MONGODB, MONGOID, NOSQL]
---

I wrote a post about how indexing with Mongoid helped us solve some problems with slow queries, enjoy!

* [Indexing with Mongoid](http://cookieshq.co.uk/posts/indexes-with-mongoid/)
