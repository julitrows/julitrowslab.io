---
layout: post
title: Creating a sitemap with Ruby on Rails and uploading it to Amazon S3
categories:
- development
tags: [cookieshq, blogging, AMAZON S3, GEM, HEROKU, RAILS, SITEMAP, SITEMAP_GENERATOR]
---

> Sitemaps are a must-have tool to get your sites properly indexed on search engines and having a better positioning. In this post we cover how to create sitemaps for your Rails apps and host them on S3 if needed.

* [Creating a sitemap with Ruby on Rails and uploading it to Amazon S3](http://cookieshq.co.uk/posts/creating-a-sitemap-with-ruby-on-rails-and-upload-it-to-amazon-s3/)
