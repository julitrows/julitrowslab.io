---
layout: post
title: It's been a while
categories:
- development
tags: [jekyll, bootstrap, blog, personal]
---

So, I was setting up again my old intel macbook from 2020 because the
security restrictions around personal usage of work laptops are
becoming a bit too uncomfortable (although standard).

Then I remembered I had this site still going, and that sometime ago I pushed
some changes that were never published. So, I started doing some changes to
the site while setting up the tools: lazyvim, wezterm, lazygit, tokyo night
and caskaydia mono nerd font.

You can tell I'm still a noob at vim because I
just wont face the work of adding links for the items in the list above haha.
Wanna save me some frustration. There's always tomorrow for that!

It's been four years or so, phew. A while indeed. Need to review
the resume content and links, and probably add up some new content as well.

I'm learning a lot about technology organizations, scale, behavior and _people_
in these latest years, but need to find the way to post without breaking any
policies.

I've updated the site to use Bootstrap 5.3 from a CDN. Wanted to try out
tailwindCSS but I saw I needed to _build_ and I said _hell naw_. Another thing
to try out _some other day_.

Also set up a new (better) pic in the home page!

Definitely should write **more**. Let's hope that happens soon™.
