---
layout: post
title: ! 'Simple good practices you’re probably not following: THE RETURN'
categories:
- development
tags:
- gem
- good practices
- project setup
- ruby on rails
status: publish
type: post
published: true
---
This one is pretty simple too, and very easy. Specify the version of the gems you are using in the Gemfile. Or in your environment.rb if you are into Rails Archaelogism. Wait, SPECIALLY if you're dealing (or know that someone will) with old projects.

Let's say you start developing a project and you want to use a gem.

{% highlight ruby %}
  gem "capistrano-tasks" # shameless plug LOL
{% endhighlight %}

At that time, you'll probably just go with whichever version rubygems throws at you. That's ok. But when you deploy into staging for the first time, make sure to note down the version of the gem that's being used in the staging machine and specify that version in the Gemfile.

{% highlight ruby %}
    gem "capistrano-tasks", "~> 0.1.0" # shameless plug LOL
{% endhighlight %}

You'll make your life a little easier when going into production for the first time because you'll be using the same gem version (and dependencies), and A LOT easier for the next guy in line trying to get the project up and running.

Be a good guy and just try to think a little in advance and be nice to the future devs, or your future self. Anyway, it seems that <a href="http://tenderlovemaking.com/2012/12/18/rails-4-and-your-gemfile.html">you're going to need to do this</a> in Rails 4.
