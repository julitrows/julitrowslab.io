---
layout: post
title: Update your Rails NOW *Updated*
categories:
- development
tags: [bugs, rails 2.3, rails 3.0, ruby on rails, security, update]
status: publish
type: post
published: true
---
It seems that all Rails branches (2.3, 3.0, 3.1 and 3.2) have been updated to fix two severe security issues (<a href="https://groups.google.com/forum/?fromgroups=#!topic/rubyonrails-security/t1WFuuQyavI">first</a> and <a href="https://groups.google.com/forum/?fromgroups=#!topic/rubyonrails-security/61bkgvnSGTQ">second</a>). Please refer to <a href="http://weblog.rubyonrails.org/2013/1/8/Rails-3-2-11-3-1-10-3-0-19-and-2-3-15-have-been-released/">the Rails Weblog post</a> for more information and take action ASAP to keep your rails apps secure!

Insight on the bug causing this 'emergency': <a href="http://www.insinuator.net/2013/01/rails-yaml/">http://www.insinuator.net/2013/01/rails-yaml/</a>

Things to have into account when updating:

### Rails 3.0.x apps over Ruby 1.8.x:

<span style="color:#ff0000;"><strong>Of course, always do this in your dev environment prior to updating your servers to make sure you're not breaking anything.</strong></span>
<ul>
	<li>Update rubygems to 1.8  (if needed) using gem update --system</li>
	<li>Modify your Gemfile to specify the rails 3.0.19 version. Note: you might have to delete your local Gemfile.lock before to update it.</li>
	<li>Run bundle.</li>
	<li>You may need to update or specify the versions of some gems. For example I found that I had the ``factory_girl_rails`` gem on my Gemfile, but its current version doesn't work with Ruby 1.8 anymore, so I had to specify the 1.7.0 version for this gem in order to keep everything working.</li>
	<li>You might want to use the gem clean command in order to remove the old rails (and other gems) versions once you've ensured that the app works after doing the previous steps. <strong>Beware, this <del>might</del> will break dependencies</strong>, run bundler afterwards to attempt to fix broken dependencies. Take extra care with your <em>passenger</em> gem, as you won't come across any problem with it until your actually working on your servers.</li>
</ul>

### Rails 2.x apps:

If you don't have bundler, install rails gem version 2.3.15, and after that, update your app/config/environment.rb file to use rails 2.3.15.

With this version of Rails I've come across this warning message when running Webrick or the Rails Console:

    NOTE: Gem.source_index is deprecated, use Specification.
    It will be removed on or after 2011-11-01.Gem.source_index called from
    /Users/meh/.rvm/gems/ruby-1.8.7-p370@project/gems/rails-2.3.15/lib/rails/gem_dependency.rb:78.

It <a href="http://stackoverflow.com/questions/6065383/gem-source-index-is-deprecated-use-specification-should-i-re-install-gem-or-ra">seems</a> that you may solve this by updating your bundler gem (``gem update bundler``) to the latest version but I found that I had to downgrade the rubygems version in that gemset to version 1.6.2  (``gem update --system 1.6.2``) to get rid of the message.
