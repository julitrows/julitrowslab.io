# julitrows.gitlab.com

This is my personal site. Contains:

* Resumé.
* Some personal info.
* An (old and kind of abandoned) technical blog.

See it live [here](http://julitrows.gitlab.io).

It's built on:

* [Bootstrap 4](https://getbootstrap.com/)
* [Jekyll 4](https://jekyllrb.com/)
* [Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/)